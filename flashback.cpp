
#include <iostream>
#include <fstream>
#include <getopt.h>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <signal.h>

using namespace std;

const int BUF_SIZE = 4 * 1024;

unsigned long diffcount(char* a, char* b, int len);
unsigned long flashback(FILE* source, FILE* destination);
void showHelp();

void signal_callback_handler(int signum) {

	printf("\nCaught signal %d\n", signum);

	exit(signum);
}

int main(int argc, char **argv) {
	// Register signal and signal handler
	signal(SIGINT, signal_callback_handler);

	cout << "flashback" << endl;

	int c;

	char* inputFile = NULL;
	char* destFileStr = NULL;

	opterr = 0;
	// Read command line arguments.
	while ((c = getopt(argc, argv, "hi:o:")) != -1)
		switch (c) {
		case 'h':
			showHelp();
			return 0;
		case 'i':
			inputFile = optarg;
			break;
		case 'o':
			destFileStr = optarg;
			break;
		case '?':
			if (optopt == 'i' || optopt == 'o')
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
			return 1;
		default:
			abort();
		}

	if (destFileStr == NULL) {
		cerr << "missing target file" << endl;
		showHelp();
		return 1;
	}

	// Open the source file.
	FILE* flashBackSource;
	const bool fromStdin = (inputFile == NULL);
	if (!fromStdin) {
		flashBackSource = fopen(inputFile, "rb");
		if (flashBackSource == NULL) {
			cerr << "could not open input stream" << endl;
			return 1;
		}

		cout << "source: " << inputFile << endl;
	}

	// Open the destination file.
	FILE* destFile;
	cout << "destination: " << destFileStr << endl;
	destFile = fopen(destFileStr, "r+b");
	if (destFile == NULL) {
		cerr << "could not open output file" << endl;
		return 1;
	}

	unsigned long totalDiff = 0;

	if (fromStdin) {
		totalDiff = flashback(stdin, destFile); // use stdin
	} else {
		totalDiff = flashback(flashBackSource, destFile);
		fclose(flashBackSource);
	}

	fflush(destFile);
	fclose(destFile);

	cout << totalDiff << " bytes updated" << endl;

	return 0;
}

// Count in how many bytes two byte strings are different.
unsigned long diffcount(char* a, char* b, int len) {
	unsigned long diff = 0;
	for (unsigned long i = 0; i < len; ++i) {
		if (*a != *b) {
			++diff;
		}
		++a;
		++b;
	}
	return diff;
}

unsigned long min(unsigned long a, unsigned long b) {
	if( a < b) return a;
	return b;
}

// Update the destination file such that its content is equal to the source file.
unsigned long flashback(FILE* source, FILE* destFile) {

	char srcBuf[BUF_SIZE];
	char destBuf[BUF_SIZE];

	unsigned long destWritePos = 0;

	unsigned long totalDiff = 0;
	
	fseek(source, 0, SEEK_END);	
	unsigned long srcSize = ftell(source);
	fseek(source, 0, SEEK_SET);
	fseek(destFile, 0, SEEK_SET);

	int len;
	bool error = false;
	while ((len = fread(srcBuf, 1, min(BUF_SIZE, srcSize-destWritePos), source)) > 0) {
		if(len == -1) {
			cerr << "io error" << endl;
			abort();
		}
		fread(destBuf, 1, BUF_SIZE, destFile);
		
		unsigned long diff = diffcount(srcBuf, destBuf, len);
		totalDiff += diff;

		if (diff != 0) {
			// Difference found in this block.
			fseek(destFile, destWritePos, SEEK_SET);
			unsigned int actual_len = fwrite(srcBuf, 1, len, destFile);
			error |= actual_len != len;
		}
		destWritePos += len;

		if (error) {
			cerr << "io error" << endl;
			abort();
		}
		fflush(destFile);

		if(destWritePos / BUF_SIZE % 32 == 0) {
			cout << "\r";
			cout << "processed: " << destWritePos/1024/1024 << " MB"
					<< "    " << "written: " << totalDiff/1024/1024 << " MB";
			fflush(stdout);
		}
	}
	cout << "\n";
	return totalDiff;
}

void showHelp() {
	cout << endl << endl;
	cout << "\tusage:\t\tflashback -o OUTPUT -i INPUT" << endl;
	cout << "\texample:\t\tflashback -o /dev/sdb -i sdb_backup.img" << endl
			<< endl;
}
