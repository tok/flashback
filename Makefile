
.PHONY: all
all: flashback

flashback: flashback.o
	g++ flashback.o -o flashback

flashback.o: flashback.cpp
	g++ -O3 -c flashback.cpp

.PHONY: clean
clean:
	rm -f *.o
	rm -f flashback
