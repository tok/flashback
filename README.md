# flashback

Simple tool to for efficiently resetting flash storage such as SD cards to a previous snapshot image. Instead of writing the full image only the blocks are written that are actually different. This can speed up the process and enhances the flash lifetime. 

## Build

```sh
git clone [this repo]
cd flashback
make
```

## Usage
```sh
# Write the image sdb_backup.img to the /dev/sdb device.
./flashback -o /dev/sdb -i sdb_backup.img
```
